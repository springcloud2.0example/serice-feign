package com.zcj.sericefeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "eureka-client", fallback = SchedualServiceHelloHystric.class)
public interface SchedualServiceHello {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    String sayHello();
}
